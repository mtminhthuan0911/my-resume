import React from "react";
import styled from "styled-components";

const MainSliderStyled = styled.div``;

function MainSlider() {
  return <MainSliderStyled className="main-slider"></MainSliderStyled>;
}

export default MainSlider;
