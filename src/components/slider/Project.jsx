import React from "react";
import styled from "styled-components";
import BK from "../../assets/img/backgroundProject.png";

const ProjectStyled = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${BK});
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  .project {
    &-container {
      max-width: 1440px;
      margin: 0 auto;
      .header {
        .title {
          font-size: 45px;
          font-weight: bold;
          color: #fff;
          padding-top: 20px;
          .txt-hightlight {
            color: #00adb5;
          }
        }
        .subTitle {
          color: #fff;
          font-size: 20px;
          font-weight: 600;
          .txt-hightlight {
            color: #00adb5;
          }
        }
      }
      .body {
        text-align: left;
        color: #fff;
        .project-items {
          padding: 20px;
          margin: 20px;
          background-color: rgba(35, 35, 35, 0.8);
          li {
            padding: 5px 0;
          }
          .txt-hightlight {
            font-weight: 600;
            color: #fff;
            &.href {
              text-decoration: none;
              color: #00adb5;
            }
          }
          .name-project {
            color: #fff;
            display: flex;
            align-items: center;
            font-size: 22px;
            font-weight: 600;
            p {
              padding: 8px;
              border-radius: 5px;
              a {
                text-decoration: none;
                color: #fff;
              }
            }
            .name {
              background-color: #00adb5;
            }
            .time {
              margin-left: auto;
              background-color: #00adb5;
            }
          }
        }
      }
    }
    @media (max-width: 920px) {
      &-container {
        .header {
          .title {
            font-size: 30px;
          }
          .subTitle {
            font-size: 16px;
          }
        }
        .body {
          .project-items {
            .name-project {
              font-size: 16px;
              display: block;
            }
          }
        }
      }
    }
  }
`;

const DataProject = [
  {
    id: 0,
    name: "ILLUON | What You See on Stage",
    position: "Frontend Developer",
    descirption:
      "Build an app for music content (musicals, operas) with a paid live viewing platform.",
    teamSize: "8",
    responsibilities: [
      { name: "Build UI / UX - Desktop and Responsive" },
      { name: "Integration API data with UI ( REST API )" },
      { name: "Integration and build Rocket Chat ( Live Chat )" },
      { name: "Integration SSR ( server-side rendering )" },
      { name: "Support integration DRM Today and Fairplay Streaming" },
      { name: "Development features others..." },
    ],
    time: "3/2022 - Now",
    linkDetail: "https://www.illuon.net/",
  },
  {
    id: 1,
    name: "CMS | Content Management System",
    position: "Frontend Developer",
    descirption:
      "CMS Build an app Content Management System , develop feature for Content Team , Admin , content creater",
    teamSize: "8",
    responsibilities: [
      { name: "Build UI / UX" },
      { name: "Integration API data with UI ( REST API )" },
      { name: "Development features" },
      { name: "Support Content Team OverView Feature" },
    ],
    time: "8/2021 - Now",
    linkDetail: "",
  },
  {
    id: 2,
    name: "SctvOnline",
    position: "Frontend Developer",
    descirption:
      "Build an app for movie content with a paid live viewing platform.",
    teamSize: "8",
    responsibilities: [
      { name: "Build UI / UX" },
      { name: "Integration API data with UI ( REST API )" },
      { name: "Development features" },
      { name: "Integration SSR ( server-side rendering )" },
      { name: "Integration Service QNET" },
    ],
    time: "10/2021 - 3/2022",
    linkDetail: "https://sctvonline.vn/",
  },
  {
    id: 3,
    name: "OndemanViet",
    position: "Frontend Developer",
    descirption:
      "Build an app for movie content with a paid live viewing platform.",
    teamSize: "8",
    responsibilities: [
      { name: "Build UI / UX" },
      { name: "Integration API data with UI ( REST API )" },
      { name: "Development features" },
      { name: "Integration SSR ( server-side rendering )" },
      { name: "Integration Service QNET" },
    ],
    time: "10/2021 - 3/2022",
    linkDetail: "https://www.ondemandviet.com/",
  },
];

function Project() {
  return (
    <ProjectStyled id="project" className="project">
      <div className="project-container">
        <div className="header">
          <div className="title">
            <p>
              Project & <span className="txt-hightlight">Experiences</span>
            </p>
          </div>
          <div className="subTitle">
            <p>
              I have <span className="txt-hightlight">2+ experience</span>{" "}
              working as frontend developer at{" "}
              <span className="txt-hightlight">ODK Media Company</span>
            </p>
          </div>
        </div>
        <div className="body">
          {DataProject.map((item, index) => (
            <div className="project-items">
              <div className="name-project">
                <p className="name">
                  <a
                    rel="noreferrer"
                    target={item.linkDetail ? "_blank" : "!#"}
                    href={item.linkDetail || "!#"}
                  >{`${index + 1} ) ${item.name}`}</a>
                </p>
                <p className="time">{item.time}</p>
              </div>
              <ul>
                <li>
                  <span className="txt-hightlight">Project Description :</span>{" "}
                  {item.descirption}
                </li>
                <li>
                  <span className="txt-hightlight">TeamSize :</span>{" "}
                  {item.teamSize}
                </li>
                <li>
                  <span className="txt-hightlight">Responsibilities :</span>{" "}
                  {item.responsibilities.map((item) => (
                    <ul>
                      <li>{item.name}</li>
                    </ul>
                  ))}
                </li>
                {item.linkDetail && (
                  <li>
                    <a
                      target="_blank"
                      rel="noreferrer"
                      href={item.linkDetail || "/"}
                      className="txt-hightlight href"
                    >
                      Link Project
                    </a>{" "}
                  </li>
                )}
              </ul>
            </div>
          ))}
        </div>
      </div>
    </ProjectStyled>
  );
}

export default Project;
