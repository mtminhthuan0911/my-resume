import React, { useRef, useState } from "react";
import styled from "styled-components";
import BG from "../../assets/img/AB.png";
import Avatar from "../../assets/img/ContactBg1.svg";
import {
  Button,
  TextField,
  Alert,
  Stack,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import emailjs from "@emailjs/browser";
import CloseIcon from "@material-ui/icons/Close";

const ContactStyled = styled.div`
  width: 100%;
  height: calc(100vh - 92px);
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${BG});
  .contact {
    &-container {
      max-width: 1440px;
      margin: 0 auto;
      display: flex;
      .left {
        text-align: left;
        color: #fff;
        width: 50%;
        img {
          border-radius: 100%;
        }
        .title {
          font-size: 45px;
          font-weight: bold;
          .txt-hightlight {
            color: #00adb5;
          }
        }
        .subTitle {
          font-size: 16px;
          font-weight: 600;
          font-style: italic;
        }
      }
      .right {
        width: 50%;
        background-color: rgba(35, 35, 35, 0.8);
        border-radius: 10px;
        padding: 30px;
        margin: 45px 30px;
        text-align: left;
        .form {
          color: #fff;
          max-width: 70%;
          fieldset,
          input {
            color: #fff;
            border-color: #fff;
            border-radius: 10px;
          }
          .info-contact {
            display: flex;
            .name-input,
            .email-input {
              .label {
                color: #00adb5;
                font-weight: 600;
              }
            }
            .email-input {
              margin-left: 50px;
            }
          }
          .info-message {
            .MuiFormControl-root {
              width: 100%;
            }
            .label {
              color: #00adb5;
              font-weight: 600;
            }
            fieldset,
            textarea {
              color: #fff;
              border-color: #fff;
            }
          }
          .action {
            margin-top: 40px;
            .btnSendMessage {
              padding: 10px 32px;
              background-color: #00adb5;
              color: #fff;
              border-radius: 10px;
              text-transform: capitalize;
              font-weight: 600;
              transition: all 0.3s;
              &:hover {
                transform: scale(1.05);
              }
            }
          }
        }
      }
    }
  }
  @media (max-width: 920px) {
    height: 100%;
    .contact {
      &-container {
        display: block;
        img {
          display: none;
        }
        .left,
        .right {
          width: 100%;
        }
        .left {
          text-align: center;
          .title {
            font-size: 35px;
          }
          .subTitle {
            font-size: 16px;
            margin: 0 20px;
          }
        }
        .right {
          margin: 0;
          padding: 0;
          .form {
            max-width: 100%;
            padding: 20px;
          }
        }
      }
    }
  }
`;

const YOUR_SERVICE_ID = "service_af3y2wv";
const YOUR_TEMPLATE_ID = "template_6hdpiyq";
const YOUR_PUBLIC_KEY = "pRSAO1aQo9FraYcLY";

function Contact() {
  const [isSuccess, setIsSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [err, setErr] = useState(false);
  const [info, setInfo] = useState({
    name: "",
    email: "",
    message: "",
  });
  const form = useRef(null);

  const onChangeMessage = (key, e) => {
    let value = e.target.value;
    setInfo({
      ...info,
      [key]: value,
    });
  };

  const onSubmitEmail = (e) => {
    e.preventDefault();
    setIsLoading(true);
    const { name, email, message } = info;
    if (!name || !email || !message) {
      setErr(true);
      setIsLoading(false);
      return;
    }
    emailjs
      .sendForm(
        YOUR_SERVICE_ID,
        YOUR_TEMPLATE_ID,
        form.current,
        YOUR_PUBLIC_KEY
      )
      .then(
        (result) => {
          console.log(result.text);
          setIsSuccess(true);
        },
        (error) => {
          console.log(error.text);
          setIsSuccess(false);
        }
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <ContactStyled id="contact" className="contact">
      <div className="contact-container">
        <div className="left">
          <div className="title">
            <p>
              Contact <span className="txt-hightlight">Me</span>
            </p>
          </div>
          <div className="subTitle">
            <p>
              "If you are an employer and my profile is suitable or exchange
              about other knowledge ."
            </p>
            <p>Please Let Me Know !</p>
          </div>
          <div className="image">
            <img width={450} height={450} src={Avatar} alt="img" />
          </div>
        </div>
        <div className="right">
          <form ref={form} onSubmit={onSubmitEmail} className="form">
            <div className="info-contact">
              <div className="name-input">
                <p className="label">Your Name</p>
                <TextField
                  onChange={(e) => onChangeMessage("name", e)}
                  name="name"
                  className="name"
                  hiddenLabel
                />
              </div>
              <div className="email-input">
                <p className="label">Email</p>
                <TextField
                  onChange={(e) => onChangeMessage("email", e)}
                  name="email"
                  className="email"
                  hiddenLabel
                />
              </div>
            </div>
            <div className="info-message">
              <p className="label">Message</p>
              <TextField
                name="message"
                onChange={(e) => onChangeMessage("message", e)}
                className="message"
                hiddenLabel
                multiline
              />
            </div>
            <div className="action">
              <Button type="submit" className="btnSendMessage">
                Send Message{" "}
                {isLoading ? (
                  <CircularProgress
                    size={25}
                    style={{ marginLeft: "10px", color: "#fff" }}
                  />
                ) : (
                  <SendIcon style={{ marginLeft: "10px" }} />
                )}
              </Button>
            </div>
          </form>
          <Stack
            sx={{ width: "100%" }}
            style={{ marginTop: "30px" }}
            spacing={2}
          >
            {isSuccess && (
              <Alert
                severity="success"
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setIsSuccess(false);
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
              >
                This is a success alert — check it out!
              </Alert>
            )}
          </Stack>
          {err && (
            <p style={{ color: "red" }}>Please fill enought information. </p>
          )}
        </div>
      </div>
    </ContactStyled>
  );
}

export default Contact;
