import React from "react";
import styled from "styled-components";
import Avatar from "../../assets/img/NewAva.png";
import { Button } from "@material-ui/core";
import { TypeAnimation } from "react-type-animation";
import BG from "../../assets/img/ABME.png";
import Myresume from "../../assets/document/Minh_Thuan_CV.pdf";
import { useMediaQuery } from "react-responsive";

const HomePageStyled = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${BG});
  .homepage {
    &-container {
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;
      position: relative;
      color: #fff;
      max-width: 1440px;
      margin: 0 auto;
      .avatar {
        position: absolute;
        bottom: -100px;
        right: 0;
      }
      img {
        border-radius: 20%;
        width: 600px;
        object-fit: cover;
      }
      p {
        text-align: left;
        margin: 0;
      }
      .info {
        width: 50%;
        &-title {
          color: #00adb5;
          font-weight: 600;
          font-size: 25px;
          text-transform: capitalize;
          padding-bottom: 20px;
          border-bottom: 1px solid #00adb5;
        }
        &-intro {
          font-size: 85px;
          font-weight: 700;
          line-height: 120%;
          padding-bottom: 20px;
          font-weight: 500;
          font-family: "Playfair Display";
        }
        &-sumary {
          font-weight: 400;
          font-size: 18px;
          line-height: 30px;
          padding-bottom: 20px;
          .bold {
            font-weight: 800;
          }
        }
        .group-button {
          padding-bottom: 20px;
          text-align: left;
          a {
            text-decoration: none;
          }
          .btn {
            padding: 8px 24px;
            margin-left: 10px;
            text-align: center;
            text-transform: capitalize;
            font-weight: 600;
            transition: all 0.3s;
            &:hover {
              transform: scale(1.1);
            }
            &.project {
              color: #fff;
              background-color: #00adb5;
            }
            &.CV {
              border: 1px solid #000;
              color: #000;
              background-color: #fff;
              margin-left: 20px;
            }
          }
        }
      }
    }
    @media (max-width: 920px) {
      &.homepage {
        height: 100%;
      }
      &-container {
        display: block;
        width: 100%;
        max-width: 100%;
        p {
          text-align: center;
        }
        .info {
          width: 100%;
          margin-top: 100px;
          position: relative;
          z-index: 2;
          &-title {
            font-size: 18px;
          }
          &-intro {
            margin-top: 30px;
            font-size: 35px;
          }
          &-sumary {
            padding: 0px 20px;
            font-size: 16px;
          }
          .group-button {
            text-align: center;
            margin-top: 10px;
            .btn {
              margin-left: 0px;
            }
          }
        }
        .avatar {
          bottom: 0;
          left: 20px;
          z-index: 1;
          img {
            max-width: 45vh;
          }
        }
      }
    }
  }
`;

function Banner() {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 920px)",
  });
  return (
    <HomePageStyled id="banner" className="homepage">
      <div className="homepage-container">
        <div className="info">
          <p className="info-title">
            {isDesktopOrLaptop ? (
              <TypeAnimation
                sequence={[
                  "frontend Developer - React Engineer",
                  700,
                  "I have 2+ years of experience as Frontend Developer",
                  700,
                  "Good knowledge of Object-Oriented and RESTful API",
                  700,
                ]}
                repeat={Infinity}
                deletionSpeed={110}
                speed={55}
              />
            ) : (
              "frontend Developer - React Engineer"
            )}
          </p>
          <p className="info-intro">I'm Minh Thuan</p>
          <p className="info-sumary">
            - Working efficiently, play hard is my work motto. To me, if we work
            together so nothing is impossible...
          </p>
          <div className="group-button">
            <a
              href="https://www.linkedin.com/in/minh-thuan-208798215/"
              target="_blank"
              rel="noreferrer"
            >
              <Button className="btn project">LinkedIn</Button>
            </a>
            <a href={Myresume} target="_blank" rel="noreferrer">
              <Button className="btn CV">My CV</Button>
            </a>
          </div>
        </div>
        <div className="avatar">
          <img src={Avatar} alt="avatar" />
        </div>
      </div>
    </HomePageStyled>
  );
}

export default Banner;
