import React, { useState } from "react";
import styled from "styled-components";
import BG from "../../assets/img/AB.png";
import LocalPhoneOutlinedIcon from "@material-ui/icons/LocalPhoneOutlined";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import DoubleArrowIcon from "@material-ui/icons/DoubleArrow";

const AboutStyled = styled.div`
  height: calc(100vh - 92px);
  width: 100%;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${BG});
  .about {
    &-container {
      max-width: 1440px;
      margin: 0 auto;
      .header {
        .title {
          font-size: 45px;
          font-weight: bold;
          color: #fff;
          .txt-hightlight {
            color: #00adb5;
          }
        }
        .subTitle {
          display: flex;
          justify-content: center;
          align-items: center;
          border: 1px solid #00adb5;
          background-color: #fff;
          font-size: 25px;
          font-weight: bold;
          border-radius: 7px;
          .tabs {
            color: #00adb5;
            width: 50%;
            cursor: pointer;
            transition: all 0.3s;
            &.active {
              background-color: #00adb5;
              color: #fff;
            }
          }
        }
        .tab-panel {
          .sumary {
            text-align: left;
            color: #fff;
            font-size: 20px;
            background-color: rgba(35, 35, 35, 0.8);
            margin: 20px 0;
            &-container {
              padding: 20px 20px;
              .text {
                span {
                  font-weight: bold;
                  color: #00adb5;
                }
              }
              .info-others {
                border-top: 1px solid #fff;
                display: flex;
                .left,
                .right {
                  width: 50%;
                }
                .text {
                  li {
                    font-weight: 600;
                    color: #00adb5;
                  }
                  p {
                    display: flex;
                    align-items: center;
                    svg {
                      padding-right: 15px;
                    }
                    a {
                      color: #00adb5;
                    }
                    .skills {
                      margin-left: auto;
                      color: #fff;
                      font-size: 18px;
                      cursor: pointer;
                      transition: all 0.3s;
                      &:hover {
                        color: #00adb5;
                      }
                    }
                  }
                }
              }
            }
          }
          .skills {
            text-align: left;
            color: #fff;
            font-size: 20px;
            background-color: rgba(35, 35, 35, 0.8);
            margin: 20px 0;
            /* border-radius: 5px; */
            &-container {
              padding: 20px 20px;
              display: flex;
              .right,
              .left,
              .bottom {
                width: 50%;
                .text {
                  li {
                    font-weight: 600;
                    color: #00adb5;
                  }
                }
              }
              .right {
                padding-left: 50px;
              }
            }
          }
        }
      }
    }
  }
  @media (max-width: 920px) {
    height: 100%;
    .about {
      &-container {
        max-width: 100%;
        padding: 0 20px;
        .header {
          .title {
            font-size: 35px;
          }
          .subTitle {
            font-size: 18px;
          }
          .tab-panel {
            .sumary {
              font-size: 16px;
              &-container {
                .info-others {
                  display: block;
                  .left,
                  .right {
                    width: 100%;
                  }
                  .text {
                    p {
                      display: block;
                    }
                    .skills {
                      display: flex;
                    }
                  }
                }
              }
            }
            .skills {
              font-size: 16px;
              &-container {
                display: block;
                .right,
                .left,
                .bottom {
                  width: 100%;
                }
                .right {
                  padding-left: 0;
                }
              }
            }
          }
        }
      }
    }
  }
`;

const arrTabPenl = [
  { id: "sumary", name: "Sumary" },
  { id: "skills", name: "Skills" },
];

function About() {
  const [active, setActive] = useState("sumary");

  const Sumary = () => {
    return (
      <div className="sumary">
        <div className="sumary-container">
          <p className="text">
            - I'm <span>Minh Thuan</span>, a guy with a lot of{" "}
            <span>creativity</span>. Learning new languages and technologies is
            something I'm passionate about.
          </p>
          <p className="text">
            - Working <span>efficiently</span>, play <span>hard</span> is my
            work motto. To me, if we work <span>together</span> so nothing is
            impossible.
          </p>
          <p className="text">
            - I have 2+ years of experience as <span>Frontend Developer</span>{" "}
            with good knowledge of Object-Oriented and RESTful API , Design
            Patterns .
          </p>
          <p className="text">
            - I'm always waiting for a job as frontend developer.
          </p>
          <div className="info-others">
            <div className="left">
              <div className="text">
                <ul>
                  <li>Education: </li>
                  <p>- Ho Chi Minh City University of Technology.</p>
                  <p>- GPA : 2.5</p>
                </ul>
              </div>
            </div>
            <div className="right">
              <div className="text">
                <ul>
                  <li>Contact info: </li>
                  <p>
                    <LocalPhoneOutlinedIcon /> 077 6156 790
                  </p>
                  <p>
                    <LinkedInIcon />{" "}
                    <a
                      href="https://www.linkedin.com/in/minh-thuan-208798215/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      mt-minhthuan
                    </a>{" "}
                    <p onClick={() => setActive("skills")} className="skills">
                      view my skills <DoubleArrowIcon />{" "}
                    </p>
                  </p>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const Skills = () => {
    return (
      <div className="skills">
        <div className="skills-container">
          <div className="left">
            <div className="text">
              <ul>
                <li>Programming Languages:</li>
                <p>- JavaScript , TypeScript </p>
                <p>- HTML5 , CSS3 , SASS , SCSS</p>
                <li> Frameworks / Libraries:</li>
                <p> - ReactJs , ReactNative</p>
                <p>- ES6 , JQuery</p>
                <p>
                  - Material UI , React Bootstrap , Styled components ,
                  Ant-design
                </p>
                <p>
                  - React Router , Create-React-App , React Hook , React Admin ,
                  Redux , Redux Thunk , Redux Saga
                </p>
              </ul>
            </div>
          </div>
          <div className="right">
            <div className="text">
              <ul>
                <li>Foreign Languages: </li>
                <p> - English : comunication basic</p>
              </ul>
              <ul>
                <li>Soft Skills:</li>
                <p>- Time management </p>
                <p>- Business communication </p>
                <p>- Teamwork</p>
              </ul>
            </div>
          </div>
          <div className="bottom">
            <div className="text">
              <ul>
                <li>Others: </li>
                <p> - Experience with Agile and Scrum development process</p>
                <p>
                  {" "}
                  - Intermediate Photoshop skill. Can do simple design: logo,
                  banner, poster….
                </p>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <AboutStyled id="about" className="about">
      <div className="about-container">
        <div className="header">
          <div className="title">
            <p>
              About <span className="txt-hightlight">me</span>
            </p>
          </div>
          <div className="subTitle">
            {arrTabPenl.map((item) => (
              <div
                onClick={() => setActive(item.id)}
                className={`tabs ${active === item.id && `active`}`}
              >
                <p>{item.name}</p>
              </div>
            ))}
          </div>
          <div className="tab-panel">
            {active === "sumary" ? <Sumary /> : <Skills />}
          </div>
        </div>
      </div>
    </AboutStyled>
  );
}

export default About;
