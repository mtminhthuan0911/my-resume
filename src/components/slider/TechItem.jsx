import React from "react";
import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import IconReact from "../../assets/icons/IconReact.png";
import IconJs from "../../assets/icons/IconJS.png";
import IconHook from "../../assets/icons/ReactHook.png";
import IconNode from "../../assets/icons/NodeJS.png";
import IconGitLab from "../../assets/icons/GitLab.png";
import IconGitHub from "../../assets/icons/GitHub.png";
import { useMediaQuery } from "react-responsive";

const TechItemStyled = styled.div`
  width: 100%;
  background-color: rgba(0, 173, 181, 0.3);
  .tech-item {
    &-container {
      max-width: 1440px;
      margin: 0 auto;
      .slider {
        padding: 20px 0;
        .swiper-wrapper {
          justify-content: center;
        }
        &-item {
          max-width: 80px;
          max-height: 80px;
          img {
            width: 100%;
            height: 100%;
            padding-left: 50px;
            filter: grayscale(1);
            cursor: pointer;
            transition: all 0.3s;
            &:hover {
              filter: grayscale(0);
              transform: scale(1.1);
            }
          }
        }
      }
    }
  }
`;

const arrIcons = [
  IconReact,
  IconJs,
  IconNode,
  IconHook,
  IconGitLab,
  IconGitHub,
];

function TechItem() {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 920px)",
  });
  if (!isDesktopOrLaptop) return null;
  return (
    <TechItemStyled className="tech-item">
      <div className="tech-item-container">
        <Swiper
          className="slider"
          modules={[Navigation]}
          spaceBetween={100}
          slidesPerView={6}
        >
          {arrIcons.map((item) => (
            <SwiperSlide className="slider-item">
              <img src={item} alt="img" />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </TechItemStyled>
  );
}

export default TechItem;
