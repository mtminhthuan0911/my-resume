import React from "react";
import styled from "styled-components";

const FooterStyled = styled.div`
  width: 100%;
  background-color: rgba(0, 173, 181, 0.3);
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  .footer {
    &-container {
      max-width: 1440px;
      margin: 0 auto;
      padding: 20px 0;
      p {
        font-weight: 400;
        color: #fff;
      }
    }
  }
`;

function Footer() {
  return (
    <FooterStyled id="footer" className="footer">
      <div className="footer-container">
        <p>Copyright 2023 - Made by Minh Thuan</p>
      </div>
    </FooterStyled>
  );
}

export default Footer;
