import React from "react";
import styled from "styled-components";
import { ReactComponent as Logo } from "../../assets/icons/Z.svg";
import { Button, Drawer } from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import MenuIcon from "@material-ui/icons/Menu";
import HomeIcon from "@material-ui/icons/Home";
import AccountTreeIcon from "@material-ui/icons/AccountTree";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import CallIcon from "@material-ui/icons/Call";

const HeaderStyled = styled.div`
  position: fixed;
  width: 100%;
  background: #222831;
  z-index: 999;
  .header {
    &-container {
      display: flex;
      align-items: center;
      justify-content: space-between;
      padding: 20px 0;
      max-width: 1440px;
      margin: 0 auto;
      .logo {
        filter: invert(1);
        cursor: pointer;
      }
      .navbar {
        display: flex;
        &-item {
          color: #fff;
          padding-right: 48px;
          font-size: 20px;
          cursor: pointer;
          font-weight: 500;
          p {
            position: relative;
            color: #fff;
            text-decoration: none;
            &:after {
              content: "";
              position: absolute;
              background-color: #00adb5;
              left: 0;
              width: 0;
              bottom: -10px;
              height: 3px;
              transition: all 0.3s;
            }
            &:hover:after {
              width: 100%;
            }
          }
        }
      }
    }
    @media (max-width: 920px) {
      &-container {
        padding: 20px 20px;
        .logo {
          transform: scale(0.8);
        }
        .menu-hambuger {
          .menu {
            font-size: 40px;
            color: #00adb5;
          }
        }
      }
    }
  }
`;

const DrawerStyled = styled(Drawer)`
  .MuiDrawer-paper {
    width: 40%;
    background-color: #222831;
    .navbar {
      margin-top: 30px;
      &-item {
        margin-bottom: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        svg {
          color: #fff;
          font-size: 30px;
        }
        p {
          color: #fff;
          font-size: 18px;
          font-weight: 500;
          text-decoration: none;
          padding-left: 10px;
          margin: 0;
        }
      }
    }
  }
`;

function Header() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 920px)",
  });
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const scrollToElement = (id) => {
    document.getElementById(`${id}`).scrollIntoView({ behavior: "smooth" });
  };

  return (
    <HeaderStyled className="header">
      <div className="header-container">
        <div onClick={() => scrollToElement("banner")} className="logo">
          <Logo />
        </div>
        {isDesktopOrLaptop ? (
          <div className="navbar">
            <div
              onClick={() => scrollToElement("banner")}
              className="navbar-item"
            >
              <p>Home</p>
            </div>
            <div
              onClick={() => scrollToElement("about")}
              className="navbar-item"
            >
              <p>About</p>
            </div>
            <div
              onClick={() => scrollToElement("project")}
              className="navbar-item"
            >
              <p>Project</p>
            </div>
            <div
              onClick={() => scrollToElement("contact")}
              className="navbar-item"
            >
              <p>Contact</p>
            </div>
          </div>
        ) : (
          <>
            <Button
              id="basic-button"
              aria-controls={open ? "basic-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleClick}
              className="menu-hambuger"
            >
              <MenuIcon className="menu" />
            </Button>
            <DrawerStyled
              className="drawer-hambuger"
              open={open}
              onClose={handleClose}
              anchor={"right"}
            >
              <div className="navbar">
                <div
                  onClick={() => scrollToElement("banner")}
                  className="navbar-item"
                >
                  <HomeIcon />
                  <p>Home</p>
                </div>
                <div
                  onClick={() => scrollToElement("about")}
                  className="navbar-item"
                >
                  <AccountCircleIcon />
                  <p>About</p>
                </div>
                <div
                  onClick={() => scrollToElement("project")}
                  className="navbar-item"
                >
                  <AccountTreeIcon />
                  <p>Project </p>
                </div>
                <div
                  onClick={() => scrollToElement("contact")}
                  className="navbar-item"
                >
                  <CallIcon />
                  <p>Contact</p>
                </div>
              </div>
            </DrawerStyled>
          </>
        )}
      </div>
    </HeaderStyled>
  );
}

export default Header;
