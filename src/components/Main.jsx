import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Header from "./header/Header";
import Banner from "./slider/Banner";
import About from "./slider/About";
import TechItem from "./slider/TechItem";
import Project from "./slider/Project";
import Contact from "./slider/Contact";
import Footer from "./Footer/Footer";
import { WifiLoader } from "react-awesome-loaders";

const MainStyled = styled.div`
  && {
    &.main {
      background: #222831;
      width: 100%;
      max-width: 100%;
      font-family: "Poppins";
      .main {
        &-container {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`;

const LoadingPageStyled = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #222831;
`;

export const BoltLoaderComponent = () => {
  return (
    <LoadingPageStyled className="loading-page">
      <WifiLoader backColor={"#E0E7FF "} frontColor={"#00adb5"} text=" " />
    </LoadingPageStyled>
  );
};

function Main() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const loadingPage = setTimeout(() => {
      setIsLoading(false);
    }, 4000);
    return () => {
      clearTimeout(loadingPage);
      setIsLoading(true);
    };
  }, []);

  if (isLoading) return <BoltLoaderComponent />;
  return (
    <MainStyled className="main">
      <Header />
      <div className="main-container">
        <Banner />
        <TechItem />
        <About />
        <Project />
        <Contact />
        <Footer />
      </div>
    </MainStyled>
  );
}

export default Main;
